# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2015, 2018.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-03 02:07+0200\n"
"PO-Revision-Date: 2018-07-14 07:32+0100\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#: youtubejobcomposite.cpp:40
#, kde-format
msgid "No YouTube account configured in your accounts."
msgstr "Nie znaleziono konta YouTube w twoich kontach."

#: youtubeplugin_config.qml:30
#, kde-format
msgid "Account:"
msgstr "Konto:"

#: youtubeplugin_config.qml:52
#, kde-format
msgid "Title:"
msgstr "Tytuł:"

#: youtubeplugin_config.qml:56
#, kde-format
msgid "Enter a title for the video..."
msgstr "Wpisz tytuł filmu..."

#: youtubeplugin_config.qml:59
#, kde-format
msgid "Tags:"
msgstr "Znaczniki:"

#: youtubeplugin_config.qml:63
#, kde-format
msgid "KDE, Kamoso"
msgstr "KDE, Kamoso"

#: youtubeplugin_config.qml:66
#, kde-format
msgid "Description:"
msgstr "Opis:"
