# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the purpose package.
#
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: purpose\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-03-06 02:34+0100\n"
"PO-Revision-Date: 2021-12-18 23:18+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 21.07.70\n"

#: reviewboardjobs.cpp:165
#, kde-format
msgid "JSON error: %1"
msgstr ""

#: reviewboardjobs.cpp:170
#, kde-format
msgid "Request Error: %1"
msgstr ""

#: reviewboardjobs.cpp:198
#, kde-format
msgid ""
"Could not create the new request:\n"
"%1"
msgstr ""

#: reviewboardjobs.cpp:237
#, kde-format
msgid "Could not upload the patch"
msgstr ""

#: reviewboardjobs.cpp:330
#, kde-format
msgid "Could not get reviews list"
msgstr ""

#: reviewboardjobs.cpp:370
#, kde-format
msgid "Could not set metadata"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: reviewboardplugin_config.qml:32 reviewpatch.ui:26
#, kde-format
msgid "Server:"
msgstr "الخادم:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: reviewboardplugin_config.qml:38 reviewpatch.ui:65
#, kde-format
msgid "Username:"
msgstr "اسم المستخدم:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: reviewboardplugin_config.qml:43 reviewpatch.ui:79
#, kde-format
msgid "Password:"
msgstr "كلمة السّرّ:"

#: reviewboardplugin_config.qml:50
#, kde-format
msgid "Repository:"
msgstr "المستودع:"

#: reviewboardplugin_config.qml:75
#, kde-format
msgid "Update Review:"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: reviewpatch.ui:17
#, kde-format
msgid "Destination"
msgstr "المقصد"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: reviewpatch.ui:36
#, kde-format
msgid "Base Dir:"
msgstr ""

#. i18n: ectx: property (text), widget (QLineEdit, basedir)
#: reviewpatch.ui:43
#, kde-format
msgid "/"
msgstr ""

#. i18n: ectx: property (placeholderText), widget (QLineEdit, basedir)
#: reviewpatch.ui:46
#, kde-format
msgid "Where this project was checked out from"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: reviewpatch.ui:56
#, kde-format
msgid "Authentication"
msgstr "الاستيثاق"

#. i18n: ectx: property (placeholderText), widget (QLineEdit, username)
#: reviewpatch.ui:72
#, kde-format
msgid "User name in the specified service"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, repositoriesBox)
#: reviewpatch.ui:99
#, kde-format
msgid "Repository"
msgstr "المستودع"

#. i18n: ectx: property (title), widget (QGroupBox, reviewsBox)
#: reviewpatch.ui:111
#, kde-format
msgid "Update review"
msgstr ""
